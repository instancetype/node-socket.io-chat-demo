/**
 * Created by instancetype on 8/18/14.
 */
 /* jshint bitwise : true, eqeqeq : true, forin : true, noarg : true, noempty : true, nonew : true,
   asi : true, esnext : true, laxcomma : true, sub : true, browser : true, node : true, phantom : true */
const
  express = require('express')
, app = express()
, http = require('http').Server(app)
, path = require('path')
, io = require('socket.io')(http)

app.use(express.static(path.join(__dirname, 'public/')))

app.get('/', function(req, res) {
  res.sendFile('index.html')
})

io.on('connection', function(socket) {

  io.emit('new user')

  socket.on('chat message', function(msg) {
    io.emit('chat message', msg)
  })

  socket.on('disconnect', function(){
    io.emit('user disconnect')
  })
})


http.listen(3000, function() {
  console.log('Server is listening on port 3000...')
})